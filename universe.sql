--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17 (Ubuntu 12.17-1.pgdg22.04+1)
-- Dumped by pg_dump version 12.17 (Ubuntu 12.17-1.pgdg22.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE universe;
--
-- Name: universe; Type: DATABASE; Schema: -; Owner: freecodecamp
--

CREATE DATABASE universe WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C.UTF-8' LC_CTYPE = 'C.UTF-8';


ALTER DATABASE universe OWNER TO freecodecamp;

\connect universe

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: galaxy; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.galaxy (
    galaxy_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description text NOT NULL,
    has_life boolean,
    year_discovered integer,
    year_since_discovery integer,
    age_in_millions_of_years numeric(10,2)
);


ALTER TABLE public.galaxy OWNER TO freecodecamp;

--
-- Name: galaxy_galaxy_id_seq; Type: SEQUENCE; Schema: public; Owner: freecodecamp
--

CREATE SEQUENCE public.galaxy_galaxy_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.galaxy_galaxy_id_seq OWNER TO freecodecamp;

--
-- Name: galaxy_galaxy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: freecodecamp
--

ALTER SEQUENCE public.galaxy_galaxy_id_seq OWNED BY public.galaxy.galaxy_id;


--
-- Name: moon; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.moon (
    moon_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description text NOT NULL,
    has_life boolean,
    year_discovered integer,
    year_since_discovery integer,
    age_in_millions_of_years numeric(10,2),
    planet_id integer
);


ALTER TABLE public.moon OWNER TO freecodecamp;

--
-- Name: moon_moon_id_seq; Type: SEQUENCE; Schema: public; Owner: freecodecamp
--

CREATE SEQUENCE public.moon_moon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.moon_moon_id_seq OWNER TO freecodecamp;

--
-- Name: moon_moon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: freecodecamp
--

ALTER SEQUENCE public.moon_moon_id_seq OWNED BY public.moon.moon_id;


--
-- Name: planet; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.planet (
    planet_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description text NOT NULL,
    has_life boolean,
    year_discovered integer,
    year_since_discovery integer,
    age_in_millions_of_years numeric(10,2),
    star_id integer
);


ALTER TABLE public.planet OWNER TO freecodecamp;

--
-- Name: planet_planet_id_seq; Type: SEQUENCE; Schema: public; Owner: freecodecamp
--

CREATE SEQUENCE public.planet_planet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planet_planet_id_seq OWNER TO freecodecamp;

--
-- Name: planet_planet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: freecodecamp
--

ALTER SEQUENCE public.planet_planet_id_seq OWNED BY public.planet.planet_id;


--
-- Name: ptype; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.ptype (
    ptype_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description text NOT NULL,
    planet_id integer
);


ALTER TABLE public.ptype OWNER TO freecodecamp;

--
-- Name: ptype_ptype_id_seq; Type: SEQUENCE; Schema: public; Owner: freecodecamp
--

CREATE SEQUENCE public.ptype_ptype_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ptype_ptype_id_seq OWNER TO freecodecamp;

--
-- Name: ptype_ptype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: freecodecamp
--

ALTER SEQUENCE public.ptype_ptype_id_seq OWNED BY public.ptype.ptype_id;


--
-- Name: star; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.star (
    star_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description text NOT NULL,
    alive boolean,
    year_discovered integer,
    year_since_discovery integer,
    age_in_millions_of_years numeric(10,2),
    galaxy_id integer
);


ALTER TABLE public.star OWNER TO freecodecamp;

--
-- Name: star_star_id_seq; Type: SEQUENCE; Schema: public; Owner: freecodecamp
--

CREATE SEQUENCE public.star_star_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.star_star_id_seq OWNER TO freecodecamp;

--
-- Name: star_star_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: freecodecamp
--

ALTER SEQUENCE public.star_star_id_seq OWNED BY public.star.star_id;


--
-- Name: galaxy galaxy_id; Type: DEFAULT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.galaxy ALTER COLUMN galaxy_id SET DEFAULT nextval('public.galaxy_galaxy_id_seq'::regclass);


--
-- Name: moon moon_id; Type: DEFAULT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.moon ALTER COLUMN moon_id SET DEFAULT nextval('public.moon_moon_id_seq'::regclass);


--
-- Name: planet planet_id; Type: DEFAULT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.planet ALTER COLUMN planet_id SET DEFAULT nextval('public.planet_planet_id_seq'::regclass);


--
-- Name: ptype ptype_id; Type: DEFAULT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.ptype ALTER COLUMN ptype_id SET DEFAULT nextval('public.ptype_ptype_id_seq'::regclass);


--
-- Name: star star_id; Type: DEFAULT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.star ALTER COLUMN star_id SET DEFAULT nextval('public.star_star_id_seq'::regclass);


--
-- Data for Name: galaxy; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.galaxy VALUES (1, '''a''', '''ok''', false, 1980, 40, 123.45);
INSERT INTO public.galaxy VALUES (2, '''b''', '''ok''', true, 1980, 40, 123.45);
INSERT INTO public.galaxy VALUES (3, '''c''', '''ok''', false, 1980, 40, 123.45);
INSERT INTO public.galaxy VALUES (4, '''d''', '''ok''', false, 1980, 40, 123.45);
INSERT INTO public.galaxy VALUES (5, '''e''', '''ok''', false, 1980, 40, 123.45);
INSERT INTO public.galaxy VALUES (6, '''f''', '''ok''', false, 1980, 40, 123.45);


--
-- Data for Name: moon; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.moon VALUES (1, '''a''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (2, '''b''', '''ok''', true, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (3, '''c''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (4, '''d''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (5, '''e''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (6, '''f''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (7, '''g''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (8, '''h''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (9, '''i''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (10, '''j''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (11, '''k''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (12, '''l''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (13, '''m''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (14, '''n''', '''ok''', true, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (15, '''o''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (16, '''p''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (17, '''q''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (18, '''r''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (19, '''s''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (20, '''t''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (21, '''u''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (22, '''v''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (23, '''w''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.moon VALUES (24, '''x''', '''ok''', false, 1980, 40, 123.45, 1);


--
-- Data for Name: planet; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.planet VALUES (1, '''a''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (2, '''b''', '''ok''', true, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (3, '''c''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (4, '''d''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (5, '''e''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (6, '''f''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (7, '''g''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (8, '''h''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (9, '''i''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (10, '''j''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (11, '''k''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.planet VALUES (12, '''l''', '''ok''', false, 1980, 40, 123.45, 1);


--
-- Data for Name: ptype; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.ptype VALUES (1, 'a', 'ok', 1);
INSERT INTO public.ptype VALUES (2, 'b', 'ok', 1);
INSERT INTO public.ptype VALUES (3, 'c', 'ok', 1);
INSERT INTO public.ptype VALUES (4, 'd', 'ok', 1);
INSERT INTO public.ptype VALUES (5, 'e', 'ok', 1);
INSERT INTO public.ptype VALUES (6, 'f', 'ok', 1);


--
-- Data for Name: star; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.star VALUES (1, '''a''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.star VALUES (2, '''b''', '''ok''', true, 1980, 40, 123.45, 1);
INSERT INTO public.star VALUES (3, '''c''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.star VALUES (4, '''d''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.star VALUES (5, '''e''', '''ok''', false, 1980, 40, 123.45, 1);
INSERT INTO public.star VALUES (6, '''f''', '''ok''', false, 1980, 40, 123.45, 1);


--
-- Name: galaxy_galaxy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: freecodecamp
--

SELECT pg_catalog.setval('public.galaxy_galaxy_id_seq', 1, false);


--
-- Name: moon_moon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: freecodecamp
--

SELECT pg_catalog.setval('public.moon_moon_id_seq', 1, false);


--
-- Name: planet_planet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: freecodecamp
--

SELECT pg_catalog.setval('public.planet_planet_id_seq', 1, false);


--
-- Name: ptype_ptype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: freecodecamp
--

SELECT pg_catalog.setval('public.ptype_ptype_id_seq', 1, false);


--
-- Name: star_star_id_seq; Type: SEQUENCE SET; Schema: public; Owner: freecodecamp
--

SELECT pg_catalog.setval('public.star_star_id_seq', 1, false);


--
-- Name: galaxy galaxy_name_key; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.galaxy
    ADD CONSTRAINT galaxy_name_key UNIQUE (name);


--
-- Name: galaxy galaxy_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.galaxy
    ADD CONSTRAINT galaxy_pkey PRIMARY KEY (galaxy_id);


--
-- Name: moon moon_name_key; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.moon
    ADD CONSTRAINT moon_name_key UNIQUE (name);


--
-- Name: moon moon_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.moon
    ADD CONSTRAINT moon_pkey PRIMARY KEY (moon_id);


--
-- Name: planet planet_name_key; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.planet
    ADD CONSTRAINT planet_name_key UNIQUE (name);


--
-- Name: planet planet_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.planet
    ADD CONSTRAINT planet_pkey PRIMARY KEY (planet_id);


--
-- Name: ptype ptype_name_key; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.ptype
    ADD CONSTRAINT ptype_name_key UNIQUE (name);


--
-- Name: ptype ptype_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.ptype
    ADD CONSTRAINT ptype_pkey PRIMARY KEY (ptype_id);


--
-- Name: star star_name_key; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.star
    ADD CONSTRAINT star_name_key UNIQUE (name);


--
-- Name: star star_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.star
    ADD CONSTRAINT star_pkey PRIMARY KEY (star_id);


--
-- Name: moon moon_planet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.moon
    ADD CONSTRAINT moon_planet_id_fkey FOREIGN KEY (planet_id) REFERENCES public.planet(planet_id);


--
-- Name: planet planet_star_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.planet
    ADD CONSTRAINT planet_star_id_fkey FOREIGN KEY (star_id) REFERENCES public.star(star_id);


--
-- Name: ptype ptype_planet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.ptype
    ADD CONSTRAINT ptype_planet_id_fkey FOREIGN KEY (planet_id) REFERENCES public.planet(planet_id);


--
-- Name: star star_galaxy_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.star
    ADD CONSTRAINT star_galaxy_id_fkey FOREIGN KEY (galaxy_id) REFERENCES public.galaxy(galaxy_id);


--
-- PostgreSQL database dump complete
--

